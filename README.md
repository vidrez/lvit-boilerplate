﻿# How To Start

### Step 0:

 - Copy .env
 
 `cp .env.example .env` 

### Step 1:

 - Install composer packages

`composer install` 
   
### Step 2:

 - Install node packages
 
`npm install`

### Step 3:

 - Generate Application Key
 
`php artisan key:generate`

### Step 4:

 - Start Up your Laravel App
 
 Ex.
 
```
 php artisan serve
 
 npm run watch-poll	
```

### Done!
![Result](https://i.ibb.co/DRt1Ftz/screen.png)
